package com.fsquiroz.movements.service.grouping;

import com.fsquiroz.movements.entity.api.MCreateGrouping;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.entity.db.GroupingElement;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.security.Authenticated;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface GroupingService {

    @PreAuthorize("isAuthenticated()")
    Page<Grouping> search(Authenticated authenticated, String term, Pageable page);

    @PostAuthorize("hasRole('PLATFORM_ADMIN') OR returnObject.owner.id == principal.user.id")
    Grouping get(Long id);

    @PreAuthorize("isAuthenticated()")
    Grouping create(MCreateGrouping toCreate, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #grouping.owner.id == principal.user.id")
    Grouping edit(MCreateGrouping toEdit, Grouping grouping, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #grouping.owner.id == principal.user.id")
    void delete(Grouping grouping, Authenticated authenticated);

    Page<Account> listAccounts(Grouping grouping, Pageable page);

    GroupingElement create(Grouping grouping, Account account, Authenticated authenticated);

    void delete(Grouping grouping, Account account);

    void accountDeleted(Account account);

    void accountRevoked(Account account, User user);

}
