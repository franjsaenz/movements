package com.fsquiroz.movements.service.grouping;

import com.fsquiroz.movements.entity.api.MCreateGrouping;
import com.fsquiroz.movements.entity.db.*;
import com.fsquiroz.movements.exception.BadRequestException;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.repository.AccountRepository;
import com.fsquiroz.movements.repository.GroupingElementRepository;
import com.fsquiroz.movements.repository.GroupingRepository;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.service.ValidationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GroupingServiceImpl implements GroupingService {

    private long maxElements;

    private ValidationService validationService;

    private AccountRepository accountRepository;

    private GroupingRepository groupingRepository;

    private GroupingElementRepository groupingElementRepository;

    public GroupingServiceImpl(
            @Value("${com.fsquiroz.movements.service.grouping.maxElements:10}") long maxElements,
            ValidationService validationService,
            AccountRepository accountRepository,
            GroupingRepository groupingRepository,
            GroupingElementRepository groupingElementRepository
    ) {
        this.maxElements = maxElements;
        this.validationService = validationService;
        this.accountRepository = accountRepository;
        this.groupingRepository = groupingRepository;
        this.groupingElementRepository = groupingElementRepository;
    }

    public Page<Grouping> search(Authenticated authenticated, String term, Pageable page) {
        if (term == null || term.isEmpty()) {
            return groupingRepository.findAllByOwner(authenticated.getUser(), page);
        } else {
            return groupingRepository.searchAllByOwner(authenticated.getUser(), term, page);
        }
    }

    public Grouping get(Long id) {
        validationService.validId(id);
        return groupingRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Grouping.class, id));
    }

    public Grouping create(MCreateGrouping toCreate, Authenticated authenticated) {
        validationService.notNull("body", toCreate);
        validationService.notEmptySize("name", 100, toCreate.getName());
        Grouping g = new Grouping();
        g.setCreated(Instant.now());
        g.setOwner(authenticated.getUser());
        g.setName(toCreate.getName());
        return groupingRepository.save(g);
    }

    public Grouping edit(MCreateGrouping toEdit, Grouping grouping, Authenticated authenticated) {
        validationService.notNull("body", toEdit);
        validationService.notEmptySize("name", 100, toEdit.getName());
        grouping.setUpdated(Instant.now());
        grouping.setEditor(authenticated.getUser());
        grouping.setName(toEdit.getName());
        return groupingRepository.save(grouping);
    }

    public void delete(Grouping grouping, Authenticated authenticated) {
        grouping.setDeleted(Instant.now());
        grouping.setEditor(authenticated.getUser());
        groupingRepository.save(grouping);
    }

    public Page<Account> listAccounts(Grouping grouping, Pageable page) {
        return accountRepository.findByGrouping(grouping, page);
    }

    public GroupingElement create(Grouping grouping, Account account, Authenticated authenticated) {
        GroupingElement el = groupingElementRepository.findByGroupingAndAccount(grouping, account)
                .orElse(new GroupingElement());
        if (el.getCreated() == null) {
            if (grouping.getElements() != null && grouping.getElements().size() >= maxElements) {
                throw BadRequestException.byExceedingMaxElements(grouping, maxElements);
            } else if (grouping.getElements() != null) {
                Set<Currency> currencies = grouping.getElements().stream()
                        .map(GroupingElement::getAccount)
                        .map(Account::getCurrency)
                        .collect(Collectors.toSet());
                if (currencies.size() >= 2 && !currencies.contains(account.getCurrency())) {
                    throw BadRequestException.byExceedingMaxCurrencies(grouping, currencies);
                }
            }
            el.setCreated(Instant.now());
            el.setOwner(authenticated.getUser());
            el.setGrouping(grouping);
            el.setAccount(account);
        } else {
            el.setUpdated(Instant.now());
            el.setEditor(authenticated.getUser());
        }
        return groupingElementRepository.save(el);
    }

    public void delete(Grouping grouping, Account account) {
        groupingElementRepository.findByGroupingAndAccount(grouping, account)
                .ifPresent(el -> groupingElementRepository.delete(el.getId()));
    }

    public void accountDeleted(Account account) {
        groupingElementRepository.delete(account);
    }

    public void accountRevoked(Account account, User user) {
        groupingElementRepository.delete(account, user);
    }

}
