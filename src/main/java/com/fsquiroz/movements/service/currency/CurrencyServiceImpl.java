package com.fsquiroz.movements.service.currency;

import com.fsquiroz.movements.entity.db.Currency;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.repository.CurrencyRepository;
import com.fsquiroz.movements.service.ValidationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private ValidationService validationService;

    private CurrencyRepository currencyRepository;

    public CurrencyServiceImpl(ValidationService validationService, CurrencyRepository currencyRepository) {
        this.validationService = validationService;
        this.currencyRepository = currencyRepository;
    }

    public Page<Currency> search(String term, Pageable page) {
        if (term == null || term.isEmpty()) {
            return currencyRepository.search(page);
        } else {
            return currencyRepository.search(term, page);
        }
    }

    public Currency get(Long id) {
        validationService.validId(id);
        return currencyRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Currency.class, id));
    }

}
