package com.fsquiroz.movements.service.currency;

import com.fsquiroz.movements.entity.db.Currency;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface CurrencyService {

    @PreAuthorize("isAuthenticated()")
    Page<Currency> search(String term, Pageable page);

    @PreAuthorize("isAuthenticated()")
    Currency get(Long id);

}
