package com.fsquiroz.movements.service.user;

import com.fsquiroz.movements.entity.api.MChangePassword;
import com.fsquiroz.movements.entity.api.MCreateUser;
import com.fsquiroz.movements.entity.api.MLogin;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.exception.AppException;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.exception.UnauthorizedException;
import com.fsquiroz.movements.integration.fsauth.AuthClient;
import com.fsquiroz.movements.integration.fsauth.api.*;
import com.fsquiroz.movements.repository.UserRepository;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.service.ValidationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.TreeMap;

@Service
public class UserServiceImpl implements UserService {

    private ValidationService validationService;

    private AuthClient authClient;

    private UserRepository userRepository;

    public UserServiceImpl(ValidationService validationService, AuthClient authClient, UserRepository userRepository) {
        this.validationService = validationService;
        this.authClient = authClient;
        this.userRepository = userRepository;
    }

    public MCredential login(MLogin login) {
        MCredential credential = authClient.login(new com.fsquiroz.movements.integration.fsauth.api.MLogin(
                login.getPlatformIdentifier(),
                login.getPassword(),
                null
        ));
        authenticate(credential);
        return credential;
    }

    private void authenticate(MCredential credential) {
        String authId = credential.getUser().getId();
        User u = userRepository.findByAuthId(authId).orElseThrow(() -> NotFoundException.byId(User.class, authId));
        if (u.getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(u);
        }
        Authenticated a = Authenticated.builder()
                .credential(credential)
                .user(u)
                .build();
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(a, null, a.getAuthorities())
        );
    }

    public Page<User> search(String term, Pageable page) {
        if (term == null || term.isEmpty()) {
            return userRepository.search(page);
        } else {
            return userRepository.search(term, page);
        }
    }

    @Transactional(rollbackFor = {AppException.class, AuthException.class, RuntimeException.class})
    public User create(MCreateUser createUser) {
        validationService.notNull("body", createUser);
        validationService.notEmptySize("email", 100, createUser.getEmail());
        validationService.notEmpty("password", createUser.getPassword());
        validationService.notEmptySize("name", 100, createUser.getName());
        User u = new User();
        u.setName(createUser.getName());
        u.setEmail(createUser.getEmail());
        u.setCreated(Instant.now());
        u = userRepository.save(u);
        MUser authUser = new MUser();
        authUser.setPassword(createUser.getPassword());
        authUser.setPlatformIdentifier(createUser.getEmail());
        authUser.setRole(Role.PLATFORM_USER);
        authUser.setAttributes(new TreeMap<>());
        authUser.getAttributes().put("originalId", u.getId());
        authUser.getAttributes().put("name", u.getName());
        authUser = authClient.createUser(authUser);
        u.setAuthId(authUser.getId());
        return userRepository.save(u);
    }

    public User get(Long id) {
        validationService.validId(id);
        return userRepository.findById(id).orElseThrow(() -> NotFoundException.byId(User.class, id));
    }

    public User edit(User user, com.fsquiroz.movements.entity.api.MUser edit, Authenticated editor) {
        validationService.notEmptySize("name", 100, edit.getName());
        validationService.editIsNotDeleted(user);
        MUser authUser = authClient.getUser(user.getAuthId(), editor.getCredential());
        if (authUser.getAttributes() == null) {
            authUser.setAttributes(new TreeMap<>());
        }
        authUser.getAttributes().put("originalId", user.getId());
        authUser.getAttributes().put("name", edit.getName());
        authClient.editUser(authUser.getId(), authUser, editor.getCredential());
        user.setName(edit.getName());
        user.setUpdated(Instant.now());
        return userRepository.save(user);
    }

    public void delete(User user, Authenticated deleter) {
        validationService.deleteIsNotDeleted("user", user);
        authClient.deleteUser(user.getAuthId(), deleter.getCredential());
        user.setDeleted(Instant.now());
        userRepository.save(user);
    }

    public void changePassword(User user, MChangePassword changePassword, Authenticated editor) {
        validationService.editIsNotDeleted(user);
        validationService.notNull("body", changePassword);
        validationService.notEmpty("oldPassword", changePassword.getOldPassword());
        validationService.notEmpty("newPassword", changePassword.getNewPassword());
        MResetPassword rp = new MResetPassword(changePassword.getOldPassword(), changePassword.getNewPassword(), null);
        authClient.updatePassword(user.getAuthId(), rp, editor.getCredential());
    }

}
