package com.fsquiroz.movements.service.user;

import com.fsquiroz.movements.entity.api.MChangePassword;
import com.fsquiroz.movements.entity.api.MCreateUser;
import com.fsquiroz.movements.entity.api.MLogin;
import com.fsquiroz.movements.entity.api.MUser;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import com.fsquiroz.movements.security.Authenticated;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface UserService {

    MCredential login(MLogin login);

    @PreAuthorize("isAuthenticated()")
    Page<User> search(String term, Pageable page);

    @PreAuthorize("hasRole('PLATFORM_ADMIN')")
    User create(MCreateUser createUser);

    @PreAuthorize("isAuthenticated()")
    User get(Long id);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #user.id == principal.user.id")
    User edit(User user, MUser edit, Authenticated editor);

    @PreAuthorize("hasRole('PLATFORM_ADMIN')")
    void delete(User user, Authenticated deleter);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #user.id == principal.user.id")
    void changePassword(User user, MChangePassword changePassword, Authenticated editor);

}
