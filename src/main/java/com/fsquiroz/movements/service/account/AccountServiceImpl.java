package com.fsquiroz.movements.service.account;

import com.fsquiroz.movements.entity.api.MAccount;
import com.fsquiroz.movements.entity.db.*;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.repository.AccountPermissionRepository;
import com.fsquiroz.movements.repository.AccountRepository;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.service.ValidationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class AccountServiceImpl implements AccountService {

    private ValidationService validationService;

    private AccountRepository accountRepository;

    private AccountPermissionRepository accountPermissionRepository;

    public AccountServiceImpl(
            ValidationService validationService,
            AccountRepository accountRepository,
            AccountPermissionRepository accountPermissionRepository
    ) {
        this.validationService = validationService;
        this.accountRepository = accountRepository;
        this.accountPermissionRepository = accountPermissionRepository;
    }

    public Page<Account> search(String term, Authenticated authenticated, Pageable page) {
        if (term == null || term.isEmpty()) {
            return accountRepository.search(authenticated.getUser(), page);
        } else {
            return accountRepository.search(authenticated.getUser(), term, page);
        }
    }

    public Account get(Long id) {
        validationService.validId(id);
        return accountRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Account.class, id));
    }

    public Account create(MAccount toCreate, Currency currency, Authenticated authenticated) {
        validationService.notNull("body", toCreate);
        validationService.notEmptySize("name", 100, toCreate.getName());
        validationService.notNull("currency", currency);
        Account a = new Account();
        a.setCreated(Instant.now());
        a.setCurrency(currency);
        a.setName(toCreate.getName());
        a.setOwner(authenticated.getUser());
        return accountRepository.save(a);
    }

    public Account edit(MAccount toEdit, Account account, Currency currency, Authenticated authenticated) {
        validationService.notNull("body", toEdit);
        validationService.notEmptySize("name", 100, toEdit.getName());
        validationService.notNull("currency", currency);
        validationService.editIsNotDeleted(account);
        account.setUpdated(Instant.now());
        account.setCurrency(currency);
        account.setName(toEdit.getName());
        account.setEditor(authenticated.getUser());
        return accountRepository.save(account);
    }

    public void delete(Account account, Authenticated authenticated) {
        validationService.deleteIsNotDeleted("account", account);
        account.setDeleted(Instant.now());
        account.setEditor(authenticated.getUser());
        accountRepository.save(account);
    }

    public Page<AccountPermission> list(Account account, Pageable page) {
        validationService.notNull("account", account);
        return accountPermissionRepository.findByAccount(account, page);
    }

    public AccountPermission grantAccess(Account account, User user, Permission permission) {
        validationService.notNull("permission", permission);
        validationService.notNull("user", user);
        validationService.editIsNotDeleted(account);
        validationService.isNotOwner(account, user);
        validationService.isNotUsingDeleted(user, account);
        AccountPermission ap = accountPermissionRepository.findById(new AccountPermission.PermissionId(account, user)).orElse(null);
        if (ap == null) {
            ap = new AccountPermission();
            ap.setId(new AccountPermission.PermissionId(account, user));
        }
        ap.setPermission(permission);
        return accountPermissionRepository.save(ap);
    }

    public void revokeAccess(Account account, User user) {
        validationService.editIsNotDeleted(account);
        AccountPermission.PermissionId id = new AccountPermission.PermissionId(account, user);
        AccountPermission ap = accountPermissionRepository.findById(id)
                .orElseThrow(() -> NotFoundException.byId(id));
        accountPermissionRepository.delete(ap.getId().getAccount(), ap.getId().getUser());
    }

}
