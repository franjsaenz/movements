package com.fsquiroz.movements.service.account;

import com.fsquiroz.movements.entity.api.MAccount;
import com.fsquiroz.movements.entity.db.*;
import com.fsquiroz.movements.security.Authenticated;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface AccountService {

    @PreAuthorize("isAuthenticated()")
    Page<Account> search(String term, Authenticated authenticated, Pageable page);

    @PostAuthorize("hasRole('PLATFORM_ADMIN') OR returnObject.owner.id == principal.user.id OR @securityCheck.canAccessAccount(returnObject)")
    Account get(Long id);

    @PreAuthorize("isAuthenticated()")
    Account create(MAccount toCreate, Currency currency, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #account.owner.id == principal.user.id")
    Account edit(MAccount toEdit, Account account, Currency currency, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #account.owner.id == principal.user.id")
    void delete(Account account, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #account.owner.id == principal.user.id")
    Page<AccountPermission> list(Account account, Pageable page);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #account.owner.id == principal.user.id")
    AccountPermission grantAccess(Account account, User user, Permission permission);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR #account.owner.id == principal.user.id OR #user.id == principal.user.id")
    void revokeAccess(Account account, User user);

}
