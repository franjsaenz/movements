package com.fsquiroz.movements.service;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class TimeUtils {

    private static final DateTimeFormatter STANDARD = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxx");

    private static final DateTimeFormatter STANDARD_ZULU = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");

    private static final DateTimeFormatter DESERIALIZE_FORMATTER = new DateTimeFormatterBuilder()
            .append(DateTimeFormatter.ISO_DATE_TIME)
            .appendPattern("[x][X]")
            .toFormatter();

    public static String standard(Instant i) {
        return STANDARD_ZULU.format(ZonedDateTime.ofInstant(i, ZoneOffset.UTC));
    }

    public static Instant standard(String s) {
        return s == null ? null : OffsetDateTime.parse(s, DESERIALIZE_FORMATTER).toInstant();
    }

}
