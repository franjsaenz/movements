package com.fsquiroz.movements.service;

import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Entity;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.exception.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

    public void notNull(String field, Object o) {
        if (o == null) {
            throw BadRequestException.byMissingParam(field);
        }
    }

    public void notEmpty(String field, String val) {
        notNull(field, val);
        if (val.isEmpty()) {
            throw BadRequestException.byEmptyParam(field);
        }
    }

    public void nullableSize(String field, int size, String val) {
        if (val != null && val.length() > size) {
            throw BadRequestException.byExceedingSize(field, size);
        }
    }

    public void notEmptySize(String field, int size, String val) {
        notEmpty(field, val);
        if (val.length() > size) {
            throw BadRequestException.byExceedingSize(field, size);
        }
    }

    public void validId(Object id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
    }

    public void editIsNotDeleted(Entity entity) {
        if (entity.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(entity);
        }
    }

    public void deleteIsNotDeleted(String field, Entity toDelete) {
        notNull(field, toDelete);
        if (toDelete.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(toDelete);
        }
    }

    public void isNotOwner(Account account, User user) {
        if (user.equals(account.getOwner())) {
            throw BadRequestException.byGrantPermissionToOwner(account, user);
        }
    }

    public void isNotUsingDeleted(Entity notDeleted, Entity target) {
        if (notDeleted.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(notDeleted, target);
        }
    }

    public void isNotUsingDeleted(Entity notDeleted) {
        if (notDeleted.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(notDeleted);
        }
    }

}
