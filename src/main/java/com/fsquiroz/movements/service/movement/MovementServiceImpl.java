package com.fsquiroz.movements.service.movement;

import com.fsquiroz.movements.entity.api.MCreateMovement;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Movement;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.repository.MovementRepository;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.service.MathUtils;
import com.fsquiroz.movements.service.ValidationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class MovementServiceImpl implements MovementService {

    private ValidationService validationService;

    private MovementRepository movementRepository;

    public MovementServiceImpl(ValidationService validationService, MovementRepository movementRepository) {
        this.validationService = validationService;
        this.movementRepository = movementRepository;
    }

    public Page<Movement> search(String term, Account account, Pageable page) {
        validationService.notNull("account", account);
        validationService.isNotUsingDeleted(account);
        if (term == null || term.isEmpty()) {
            return movementRepository.search(account, page);
        } else {
            return movementRepository.search(account, term, page);
        }
    }

    public Movement get(Long id) {
        validationService.validId(id);
        return movementRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Movement.class, id));
    }

    public Movement create(MCreateMovement toCreate, Account account, Authenticated authenticated) {
        validationService.notNull("body", toCreate);
        validationService.notNull("registered", toCreate.getRegistered());
        validationService.notNull("amount", toCreate.getAmount());
        validationService.notNull("debit", toCreate.getDebit());
        validationService.notNull("account", account);
        validationService.nullableSize("description", 100, toCreate.getDescription());
        validationService.isNotUsingDeleted(account);
        Movement m = new Movement();
        m.setCreated(Instant.now());
        m.setOwner(authenticated.getUser());
        m.setRegistered(toCreate.getRegistered());
        m.setAccount(account);
        m.setDescription(toCreate.getDescription());
        if (toCreate.getDebit()) {
            m.setDebit(MathUtils.round(toCreate.getAmount()));
        } else {
            m.setCredit(MathUtils.round(toCreate.getAmount()));
        }
        return movementRepository.save(m);
    }

    public Movement edit(MCreateMovement toEdit, Movement movement, Authenticated authenticated) {
        validationService.notNull("body", toEdit);
        validationService.notNull("registered", toEdit.getRegistered());
        validationService.notNull("amount", toEdit.getAmount());
        validationService.notNull("debit", toEdit.getDebit());
        validationService.nullableSize("description", 100, toEdit.getDescription());
        validationService.editIsNotDeleted(movement);
        validationService.editIsNotDeleted(movement.getAccount());
        movement.setUpdated(Instant.now());
        movement.setEditor(authenticated.getUser());
        movement.setRegistered(toEdit.getRegistered());
        movement.setDescription(toEdit.getDescription());
        if (toEdit.getDebit() != null && toEdit.getDebit()) {
            movement.setDebit(MathUtils.round(toEdit.getAmount()));
            movement.setCredit(null);
        } else {
            movement.setCredit(MathUtils.round(toEdit.getAmount()));
            movement.setDebit(null);
        }
        return movementRepository.save(movement);
    }

    public void delete(Movement movement, Authenticated authenticated) {
        validationService.deleteIsNotDeleted("movement", movement);
        movement.setDeleted(Instant.now());
        movement.setEditor(authenticated.getUser());
        movementRepository.save(movement);
    }

}
