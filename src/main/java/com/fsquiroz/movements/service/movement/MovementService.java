package com.fsquiroz.movements.service.movement;

import com.fsquiroz.movements.entity.api.MCreateMovement;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Movement;
import com.fsquiroz.movements.security.Authenticated;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface MovementService {

    @PreAuthorize("isAuthenticated()")
    Page<Movement> search(String term, Account account, Pageable page);

    @PostAuthorize("hasRole('PLATFORM_ADMIN') OR returnObject.account.owner.id == principal.user.id OR @securityCheck.canAccessAccount(returnObject.account)")
    Movement get(Long id);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR @securityCheck.canWriteAccount(#account)")
    Movement create(MCreateMovement toCreate, Account account, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR @securityCheck.canWriteAccount(#movement.account)")
    Movement edit(MCreateMovement toEdit, Movement movement, Authenticated authenticated);

    @PreAuthorize("hasRole('PLATFORM_ADMIN') OR @securityCheck.canWriteAccount(#movement.account)")
    void delete(Movement movement, Authenticated authenticated);

}
