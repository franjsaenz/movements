package com.fsquiroz.movements.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthorizationFilter extends BasicAuthenticationFilter {

    private SecurityCheck securityCheck;

    public RestAuthorizationFilter(SecurityCheck securityCheck) {
        super((Authentication authentication) -> {
            throw new UnsupportedOperationException("Not supported yet.");
        });
        this.securityCheck = securityCheck;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");
        if (header == null) {
            chain.doFilter(request, response);
            return;
        }
        UsernamePasswordAuthenticationToken auth = getAuth(header);
        if (auth == null) {
            SecurityContextHolder.clearContext();
            chain.doFilter(request, response);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(auth);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuth(String header) {
        Assert.hasText(header, "'header' must not be empty");
        try {
            Authenticated a = securityCheck.authenticate(header);
            if (a != null) {
                return new UsernamePasswordAuthenticationToken(a, null, a.getAuthorities());
            }
        } catch (Exception ignored) {
        }
        return null;
    }

}
