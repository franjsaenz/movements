package com.fsquiroz.movements.security;

import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Authenticated implements UserDetails {

    private MCredential credential;

    private User user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (credential != null && credential.getUser() != null && credential.getUser().getRole() != null) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + credential.getUser().getRole().getName()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return credential != null && credential.getUser() != null ? credential.getUser().getPlatformIdentifier() : null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return user != null && credential != null && user.getDeleted() != null && (credential.getExpiration() == null || credential.getExpiration().isAfter(Instant.now()));
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonExpired();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isAccountNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return isAccountNonExpired();
    }

}
