package com.fsquiroz.movements.security;

import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Permission;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.exception.BadRequestException;
import com.fsquiroz.movements.exception.ForbiddenException;
import com.fsquiroz.movements.exception.NotFoundException;
import com.fsquiroz.movements.exception.UnauthorizedException;
import com.fsquiroz.movements.integration.fsauth.AuthClient;
import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import com.fsquiroz.movements.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SecurityCheck {

    private AuthClient authClient;

    private UserRepository userRepository;

    public SecurityCheck(
            AuthClient authClient,
            UserRepository userRepository
    ) {
        this.authClient = authClient;
        this.userRepository = userRepository;
    }

    public User get(String authId) {
        User u = userRepository.findByAuthId(authId).orElseThrow(() -> NotFoundException.byId(User.class, authId));
        if (u.getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(u);
        }
        return u;
    }

    public Authenticated authenticate(String token) {
        MCredential credential = new MCredential();
        credential.setToken(token);
        try {
            credential = authClient.getLoggedIn(credential);
        } catch (AuthException ae) {
            switch (ae.getStatus()) {
                case 401:
                    throw UnauthorizedException.fromAuthClient(ae);
                case 403:
                    throw ForbiddenException.fromAuthClient(ae);
                default:
                    throw BadRequestException.fromAuthClient(ae);
            }
        }
        credential.setToken(token);
        User u = credential.getUser() != null ? get(credential.getUser().getId()) : null;
        return Authenticated.builder()
                .credential(credential)
                .user(u)
                .build();
    }

    public Authenticated getAuthentication() {
        return getAuthenticated();
    }

    public static Authenticated getAuthenticated() {
        Authenticated authenticated = null;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication a = context.getAuthentication();
            if (a != null) {
                Object o = a.getPrincipal();
                if (o instanceof Authenticated) {
                    authenticated = (Authenticated) o;
                }
            }
        }
        if (authenticated == null) {
            throw UnauthorizedException.byInsufficientPermission();
        }
        return authenticated;
    }

    public boolean canAccessAccount(Account account) {
        Authenticated authenticated = getAuthentication();
        return account.getPermissions().stream().anyMatch(ap -> authenticated.getUser().equals(ap.getId().getUser()));
    }

    public boolean canWriteAccount(Account account) {
        Authenticated authenticated = getAuthentication();
        return authenticated.getUser().equals(account.getOwner()) || account.getPermissions().stream().anyMatch(
                ap -> Permission.WRITE.equals(ap.getPermission()) && authenticated.getUser().equals(ap.getId().getUser())
        );
    }

}
