package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.MCurrency;
import com.fsquiroz.movements.entity.api.MException;
import com.fsquiroz.movements.entity.db.Currency;
import com.fsquiroz.movements.mapper.CurrencyMapper;
import com.fsquiroz.movements.service.currency.CurrencyService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/currencies")
@ApiOperation(value = "Currency Controller", notes = "All operations related to currencies", nickname = "currency-controller")
public class CurrencyController {

    private CurrencyService currencyService;

    private CurrencyMapper currencyMapper;

    public CurrencyController(CurrencyService currencyService, CurrencyMapper currencyMapper) {
        this.currencyService = currencyService;
        this.currencyMapper = currencyMapper;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Search currencies", notes = "Allow to list all currencies or search by name and code", nickname = "list-currency")
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        Page<Currency> currencies = currencyService.search(term, pageable);
        return ResponseEntity.ok(currencyMapper.map(currencies));
    }

    @GetMapping("/{currencyId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MCurrency.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Get currency", notes = "Get a single currency by id", nickname = "get-currency")
    public ResponseEntity<?> get(@PathVariable("currencyId") Long currencyId) {
        Currency currency = currencyService.get(currencyId);
        return ResponseEntity.ok(currencyMapper.map(currency));
    }

}
