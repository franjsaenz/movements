package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.*;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.entity.db.GroupingElement;
import com.fsquiroz.movements.mapper.AccountMapper;
import com.fsquiroz.movements.mapper.GroupingMapper;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.account.AccountService;
import com.fsquiroz.movements.service.grouping.GroupingService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/groupings")
@ApiOperation(value = "Grouping Controller", notes = "All operations related to account grouping", nickname = "grouping-controller")
public class GroupingController {

    private SecurityCheck securityCheck;

    private AccountService accountService;

    private GroupingService groupingService;

    private AccountMapper accountMapper;

    private GroupingMapper groupingMapper;

    public GroupingController(
            SecurityCheck securityCheck,
            AccountService accountService,
            GroupingService groupingService,
            AccountMapper accountMapper,
            GroupingMapper groupingMapper
    ) {
        this.securityCheck = securityCheck;
        this.accountService = accountService;
        this.groupingService = groupingService;
        this.accountMapper = accountMapper;
        this.groupingMapper = groupingMapper;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Search groupings", notes = "Allow to list all groupings or search by name", nickname = "list-grouping")
    public ResponseEntity<Page<MGrouping>> search(
            @RequestParam(required = false) String term,
            @ApiIgnore Pageable pageable
    ) {
        Authenticated authenticated = securityCheck.getAuthentication();
        Page<Grouping> groupings = groupingService.search(authenticated, term, pageable);
        return ResponseEntity.ok(
                groupingMapper.map(groupings)
        );
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MGrouping.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Create grouping", notes = "Create a single grouping", nickname = "create-grouping")
    public ResponseEntity<MGrouping> create(@RequestBody MCreateGrouping toCreate) {
        Authenticated authenticated = securityCheck.getAuthentication();
        Grouping grouping = groupingService.create(toCreate, authenticated);
        return ResponseEntity.ok(
                groupingMapper.map(grouping)
        );
    }

    @GetMapping("/{groupingId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MGrouping.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Get grouping", notes = "Get a single grouping by id", nickname = "get-grouping")
    public ResponseEntity<MGrouping> get(@PathVariable("groupingId") Long groupingId) {
        Grouping grouping = groupingService.get(groupingId);
        return ResponseEntity.ok(
                groupingMapper.map(grouping)
        );
    }

    @PutMapping("/{groupingId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MGrouping.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Update grouping", notes = "Update a single grouping's information", nickname = "update-grouping")
    public ResponseEntity<MGrouping> update(
            @PathVariable("groupingId") Long groupingId,
            @RequestBody MCreateGrouping toEdit
    ) {
        Authenticated authenticated = securityCheck.getAuthentication();
        Grouping grouping = groupingService.get(groupingId);
        grouping = groupingService.edit(toEdit, grouping, authenticated);
        return ResponseEntity.ok(
                groupingMapper.map(grouping)
        );
    }

    @DeleteMapping("/{groupingId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Delete grouping", notes = "Delete a single grouping from the app", nickname = "delete-grouping")
    public ResponseEntity<?> delete(@PathVariable("groupingId") Long groupingId) {
        Authenticated authenticated = securityCheck.getAuthentication();
        Grouping grouping = groupingService.get(groupingId);
        groupingService.delete(grouping, authenticated);
        return ResponseEntity.ok(MResponse.of("Grouping deleted"));
    }

    @GetMapping("/{groupingId}/accounts")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "List grouping's accounts", notes = "Allow to list all grouping's accounts", nickname = "list-grouping-accounts")
    public ResponseEntity<Page<MAccount>> listAccounts(
            @PathVariable("groupingId") Long groupingId,
            @ApiIgnore Pageable pageable
    ) {
        Grouping grouping = groupingService.get(groupingId);
        Page<Account> accounts = groupingService.listAccounts(grouping, pageable);
        return ResponseEntity.ok(
                accountMapper.map(accounts)
        );
    }

    @PostMapping("/{groupingId}/accounts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MAccount.class),
            @ApiResponse(code = 201, message = "Created", response = MAccount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Add grouping's account", notes = "Add a single account to a grouping", nickname = "add-account-grouping")
    public ResponseEntity<MAccount> add(
            @PathVariable("groupingId") Long groupingId,
            @RequestParam Long accountId
    ) {
        Authenticated authenticated = securityCheck.getAuthentication();
        Grouping grouping = groupingService.get(groupingId);
        Account account = accountService.get(accountId);
        GroupingElement el = groupingService.create(grouping, account, authenticated);
        return new ResponseEntity<>(
                accountMapper.map(el.getAccount()),
                el.getUpdated() == null ? HttpStatus.CREATED : HttpStatus.OK
        );
    }

    @DeleteMapping("/{groupingId}/accounts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Remove grouping's account", notes = "Remove a single account from a grouping", nickname = "remove-account-grouping")
    public ResponseEntity<MResponse> remove(
            @PathVariable("groupingId") Long groupingId,
            @RequestParam Long accountId
    ) {
        Grouping grouping = groupingService.get(groupingId);
        Account account = accountService.get(accountId);
        groupingService.delete(grouping, account);
        return ResponseEntity.ok(MResponse.of("Account removed"));
    }

}
