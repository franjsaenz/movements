package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.*;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Movement;
import com.fsquiroz.movements.mapper.MovementMapper;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.account.AccountService;
import com.fsquiroz.movements.service.movement.MovementService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/movements")
@ApiOperation(value = "Movement Controller", notes = "All operations related to movements", nickname = "movement-controller")
public class MovementController {

    private AccountService accountService;

    private MovementService movementService;

    private MovementMapper movementMapper;

    public MovementController(AccountService accountService, MovementService movementService, MovementMapper movementMapper) {
        this.accountService = accountService;
        this.movementService = movementService;
        this.movementMapper = movementMapper;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Search movements", notes = "Allow to list all movements or search by name", nickname = "list-movements")
    public ResponseEntity<?> search(
            @RequestParam(required = false) String term,
            @RequestParam Long accountId,
            @ApiIgnore Pageable pageable
    ) {
        Account account = accountService.get(accountId);
        Page<Movement> movements = movementService.search(term, account, pageable);
        return ResponseEntity.ok(movementMapper.map(movements));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MMovement.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Create movement", notes = "Create a single movement", nickname = "create-movement")
    public ResponseEntity<?> create(
            @RequestBody MCreateMovement toCreate,
            @RequestParam Long accountId
    ) {
        Account account = accountService.get(accountId);
        Movement movement = movementService.create(toCreate, account, SecurityCheck.getAuthenticated());
        return new ResponseEntity<>(movementMapper.map(movement), HttpStatus.CREATED);
    }

    @GetMapping("/{movementId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MMovement.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Get movement", notes = "Get a single movement by id", nickname = "get-movement")
    public ResponseEntity<?> get(@PathVariable("movementId") Long movementId) {
        Movement movement = movementService.get(movementId);
        return ResponseEntity.ok(movementMapper.map(movement));
    }

    @PutMapping("/{movementId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MMovement.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Update movement", notes = "Update a single movement's information", nickname = "update-movement")
    public ResponseEntity<?> update(@PathVariable("movementId") Long movementId, @RequestBody MCreateMovement toEdit) {
        Movement movement = movementService.get(movementId);
        movement = movementService.edit(toEdit, movement, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(movementMapper.map(movement));
    }

    @DeleteMapping("/{movementId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Delete movement", notes = "Delete a single movement from the app", nickname = "delete-movement")
    public ResponseEntity<?> delete(@PathVariable("movementId") Long movementId) {
        Movement movement = movementService.get(movementId);
        movementService.delete(movement, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(MResponse.of("Movement deleted"));
    }

}
