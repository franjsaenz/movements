package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.*;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.mapper.UserMapper;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.user.UserService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/users")
@ApiOperation(value = "User Controller", notes = "All operations related to users", nickname = "user-controller")
public class UserController {

    private UserService userService;

    private UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Search user", notes = "Allow to list all user or search by name", nickname = "list-users")
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        Page<User> users = userService.search(term, pageable);
        return ResponseEntity.ok(userMapper.map(users));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Create user", notes = "Create a single user", nickname = "create-user")
    public ResponseEntity<?> create(@RequestBody MCreateUser createUser) {
        User user = userService.create(createUser);
        return new ResponseEntity<>(userMapper.map(user), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Get user", notes = "Get a single user by id", nickname = "get-user")
    public ResponseEntity<?> get(@PathVariable("userId") Long userId) {
        User user = userService.get(userId);
        return ResponseEntity.ok(userMapper.map(user));
    }

    @PutMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Update user", notes = "Update a single user's information", nickname = "update-user")
    public ResponseEntity<?> update(@PathVariable("userId") Long userId, @RequestBody MUser edit) {
        User user = userService.get(userId);
        user = userService.edit(user, edit, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(userMapper.map(user));
    }

    @DeleteMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Delete user", notes = "Delete a single user from the app", nickname = "delete-user")
    public ResponseEntity<?> delete(@PathVariable("userId") Long userId) {
        User user = userService.get(userId);
        userService.delete(user, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(MResponse.of("User deleted"));
    }

    @PutMapping("/{userId}/password")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Change password", notes = "Change a user's password", nickname = "change-password")
    public ResponseEntity<?> changePassword(@PathVariable("userId") Long userId, @RequestBody MChangePassword changePassword) {
        User user = userService.get(userId);
        userService.changePassword(user, changePassword, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(MResponse.of("Password changed"));
    }

}
