package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.MAccount;
import com.fsquiroz.movements.entity.api.MAccountPermission;
import com.fsquiroz.movements.entity.api.MException;
import com.fsquiroz.movements.entity.api.MResponse;
import com.fsquiroz.movements.entity.db.*;
import com.fsquiroz.movements.mapper.AccountMapper;
import com.fsquiroz.movements.mapper.AccountPermissionMapper;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.account.AccountService;
import com.fsquiroz.movements.service.currency.CurrencyService;
import com.fsquiroz.movements.service.grouping.GroupingService;
import com.fsquiroz.movements.service.user.UserService;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/accounts")
@ApiOperation(value = "Account Controller", notes = "All operations related to accounts", nickname = "account-controller")
public class AccountController {

    private AccountService accountService;

    private UserService userService;

    private CurrencyService currencyService;

    private GroupingService groupingService;

    private AccountMapper accountMapper;

    private AccountPermissionMapper accountPermissionMapper;

    public AccountController(
            AccountService accountService,
            UserService userService,
            CurrencyService currencyService,
            GroupingService groupingService,
            AccountMapper accountMapper,
            AccountPermissionMapper accountPermissionMapper
    ) {
        this.accountService = accountService;
        this.userService = userService;
        this.currencyService = currencyService;
        this.groupingService = groupingService;
        this.accountMapper = accountMapper;
        this.accountPermissionMapper = accountPermissionMapper;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Search accounts", notes = "Allow to list all accounts or search by name", nickname = "list-accounts")
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        Page<Account> accounts = accountService.search(term, SecurityCheck.getAuthenticated(), pageable);
        return ResponseEntity.ok(accountMapper.map(accounts));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MAccount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Create account", notes = "Create a single account", nickname = "create-account")
    public ResponseEntity<?> create(
            @RequestBody MAccount account,
            @RequestParam Long currencyId
    ) {
        Currency currency = currencyService.get(currencyId);
        Account a = accountService.create(account, currency, SecurityCheck.getAuthenticated());
        MAccount ma = accountMapper.map(a);
        return new ResponseEntity<>(ma, HttpStatus.CREATED);
    }

    @GetMapping("/{accountId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MAccount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Get account", notes = "Get a single account by id", nickname = "get-account")
    public ResponseEntity<?> get(@PathVariable("accountId") Long accountId) {
        Account account = accountService.get(accountId);
        return ResponseEntity.ok(accountMapper.map(account));
    }

    @PutMapping("/{accountId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MAccount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Update account", notes = "Update a single account's information", nickname = "update-account")
    public ResponseEntity<?> update(
            @PathVariable("accountId") Long accountId,
            @RequestBody MAccount edit,
            @RequestParam Long currencyId
    ) {
        Account account = accountService.get(accountId);
        Currency currency = currencyService.get(currencyId);
        account = accountService.edit(edit, account, currency, SecurityCheck.getAuthenticated());
        return ResponseEntity.ok(accountMapper.map(account));
    }

    @DeleteMapping("/{accountId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "Delete account", notes = "Delete a single account from the app", nickname = "delete-account")
    public ResponseEntity<?> delete(@PathVariable("accountId") Long accountId) {
        Account account = accountService.get(accountId);
        accountService.delete(account, SecurityCheck.getAuthenticated());
        groupingService.accountDeleted(account);
        return ResponseEntity.ok(MResponse.of("Account deleted"));
    }

    @GetMapping("/{accountId}/permissions")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @ApiOperation(value = "List account's permissions", notes = "Allow to list all account's permissions", nickname = "list-accounts-permissions")
    public ResponseEntity<?> listPermissions(@PathVariable("accountId") Long accountId, @ApiIgnore Pageable pageable) {
        Account account = accountService.get(accountId);
        Page<AccountPermission> permissions = accountService.list(account, pageable);
        return ResponseEntity.ok(accountPermissionMapper.map(permissions));
    }

    @PostMapping("/{accountId}/permissions")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MAccountPermission.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Grant permission", notes = "Grant permissions to a user for an account", nickname = "create-account-permission")
    public ResponseEntity<?> grantPermission(
            @PathVariable("accountId") Long accountId,
            @RequestParam Long userId,
            @RequestParam Permission permission
    ) {
        Account account = accountService.get(accountId);
        User user = userService.get(userId);
        AccountPermission ap = accountService.grantAccess(account, user, permission);
        MAccountPermission map = accountPermissionMapper.map(ap);
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @DeleteMapping("/{accountId}/permissions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    @ApiOperation(value = "Revoke permission", notes = "Revoke permissions to a user for an account", nickname = "delete-account-permission")
    public ResponseEntity<?> revokePermission(
            @PathVariable("accountId") Long accountId,
            @RequestParam Long userId
    ) {
        Account account = accountService.get(accountId);
        User user = userService.get(userId);
        accountService.revokeAccess(account, user);
        groupingService.accountRevoked(account, user);
        return ResponseEntity.ok(MResponse.of("Permission revoked"));
    }

}
