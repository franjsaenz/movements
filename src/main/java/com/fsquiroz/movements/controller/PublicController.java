package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.api.MException;
import com.fsquiroz.movements.entity.api.MLogin;
import com.fsquiroz.movements.entity.api.MStatus;
import com.fsquiroz.movements.integration.fsauth.AuthClient;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import com.fsquiroz.movements.mapper.CredentialMapper;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.user.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class PublicController {

    private MStatus status;

    private SecurityCheck securityCheck;

    private UserService userService;

    private CredentialMapper credentialMapper;

    public PublicController(
            MStatus status,
            SecurityCheck securityCheck,
            UserService userService,
            CredentialMapper credentialMapper
    ) {
        this.status = status;
        this.securityCheck = securityCheck;
        this.userService = userService;
        this.credentialMapper = credentialMapper;
    }

    @GetMapping
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MStatus.class)
    })
    public ResponseEntity<?> index() {
        return ResponseEntity.ok(status);
    }

    @PostMapping("/login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MCredential.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class)
    })
    public ResponseEntity<?> login(
            @RequestBody MLogin login
    ) {
        MCredential credential = userService.login(login);
        return ResponseEntity.ok(
                credentialMapper.map(credential)
        );
    }

    @GetMapping("/login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MCredential.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class)
    })
    public ResponseEntity<?> loggedIn() {
        Authenticated a = securityCheck.getAuthentication();
        return ResponseEntity.ok(
                credentialMapper.map(a.getCredential())
        );
    }

}
