package com.fsquiroz.movements.controller;

import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.entity.api.MException;
import com.fsquiroz.movements.exception.*;
import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice(annotations = RestController.class)
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private SecurityCheck securityCheck;

    public ApiExceptionHandler(SecurityCheck securityCheck) {
        this.securityCheck = securityCheck;
    }

    @ExceptionHandler(AppException.class)
    public ResponseEntity<Object> appException(HttpServletRequest req, AppException ae) {
        logUrl(req);
        log.debug(ae.getMessage(), ae);
        return parseException(req, ae);
    }

    @ExceptionHandler(AuthException.class)
    public ResponseEntity<?> authException(HttpServletRequest req, AuthException ae) {
        AppException appException;
        switch (ae.getStatus()) {
            case 401:
                appException = UnauthorizedException.fromAuthClient(ae);
                break;
            case 403:
                appException = ForbiddenException.fromAuthClient(ae);
                break;
            default:
                appException = BadRequestException.fromAuthClient(ae);
                break;
        }
        return appException(req, appException);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<Object> propertyReferenceException(HttpServletRequest req, PropertyReferenceException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byInvalidSortParam(e.getType().getType(), e.getPropertyName()));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.debug(e.getMessage(), e);
        return parseException(null, BadRequestException.byMissingParam(e.getParameterName()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.debug(e.getMessage(), e);
        return parseException(null, BadRequestException.byMalformedBody());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> typeMismatchException(HttpServletRequest req, MethodArgumentTypeMismatchException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMalformedParam(e));
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<Object> dataAccessApiUsageException(HttpServletRequest req, InvalidDataAccessApiUsageException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMalformedSortingParam(e.getMessage()));
    }

    @ExceptionHandler(ClientAbortException.class)
    public void clientAbortException(HttpServletRequest req, ClientAbortException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> accessDeniedException(HttpServletRequest req, AccessDeniedException e) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        ErrorCode errorCode;
        Map<String, Object> meta = null;
        try {
            Authenticated a = securityCheck.getAuthentication();
            meta = new LinkedHashMap<>();
            Map<String, Object> accessAs = new LinkedHashMap<>();
            accessAs.put("userId", a.getUser() != null ? a.getUser().getId() : null);
            accessAs.put("userName", a.getUser() != null ? a.getUser().getName() : null);
            accessAs.put("userRole", a.getCredential() != null && a.getCredential().getUser() != null ? a.getCredential().getUser().getRole().getName() : null);
            meta.put("accessingAs", accessAs);
            errorCode = ErrorCode.FORBIDDEN;
        } catch (AppException ae) {
            status = HttpStatus.UNAUTHORIZED;
            errorCode = ErrorCode.U_ACCESS_DENIED;
        }
        logUrl(req);
        return build(req, status, errorCode, meta, e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> genericException(HttpServletRequest req, Exception e) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.error("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.error("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        log.error(e.getMessage(), e);
        return parseException(req, e);
    }

    private void logUrl(HttpServletRequest req) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.debug("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.debug("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

    private ResponseEntity<Object> parseException(HttpServletRequest req, Exception e) {
        ResponseStatus rs = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), ResponseStatus.class);
        HttpStatus status;
        Map<String, Object> meta = null;
        ErrorCode errorCode = ErrorCode.INTERNAL_SERVER;
        if (rs != null) {
            status = rs.value();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (e instanceof AppException) {
            AppException ae = (AppException) e;
            meta = ae.getMeta();
            errorCode = ae.getCode();
        }
        return build(req, status, errorCode, meta, status == HttpStatus.INTERNAL_SERVER_ERROR ? "There has been an unexpected error" : e.getMessage());
    }

    private ResponseEntity<Object> build(HttpServletRequest req, HttpStatus status, ErrorCode errorCode, Map<String, Object> meta, String message) {
        MException me = new MException(
                Instant.now(),
                status.value(),
                status.getReasonPhrase(),
                message,
                req != null ? req.getRequestURI() : null,
                meta,
                errorCode
        );
        return new ResponseEntity<>(
                me,
                status
        );
    }

}
