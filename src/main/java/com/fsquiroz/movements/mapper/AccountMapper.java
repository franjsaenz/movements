package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MAccount;
import com.fsquiroz.movements.entity.api.MCurrency;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Permission;
import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.repository.AccountRepository;
import com.fsquiroz.movements.security.SecurityCheck;
import com.fsquiroz.movements.service.MathUtils;
import org.springframework.stereotype.Service;

@Service
public class AccountMapper extends Mapper<Account, MAccount> {

    private UserMapper userMapper;

    private CurrencyMapper currencyMapper;

    private AccountRepository accountRepository;

    public AccountMapper(UserMapper userMapper, CurrencyMapper currencyMapper, AccountRepository accountRepository) {
        this.userMapper = userMapper;
        this.currencyMapper = currencyMapper;
        this.accountRepository = accountRepository;
    }

    @Override
    public MAccount map(Account account) {
        User user = SecurityCheck.getAuthenticated().getUser();
        MCurrency c = currencyMapper.map(account.getCurrency());
        MAccount a = new MAccount();
        a.setId(account.getId());
        a.setCreated(account.getCreated());
        a.setUpdated(account.getUpdated());
        a.setDeleted(account.getDeleted());
        a.setCurrency(c);
        a.setBalance(MathUtils.round(accountRepository.getTotal(account)));
        a.setName(account.getName());
        a.setCanEdit(user.equals(account.getOwner()) || account.getPermissions().stream().anyMatch(
                p -> Permission.WRITE.equals(p.getPermission()) && user.equals(p.getId().getUser())
        ));
        a.setOwner(user.equals(account.getOwner()));
        a.setCreator(userMapper.map(account.getOwner()));
        return a;
    }

}
