package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MGrouping;
import com.fsquiroz.movements.entity.api.MGroupingBalance;
import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.repository.GroupingElementRepository;
import com.fsquiroz.movements.service.MathUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class GroupingMapper extends Mapper<Grouping, MGrouping> {

    private GroupingElementRepository groupingElementRepository;

    public GroupingMapper(GroupingElementRepository groupingElementRepository) {
        this.groupingElementRepository = groupingElementRepository;
    }

    @Override
    public MGrouping map(Grouping grouping) {
        MGrouping g = new MGrouping();
        g.setId(grouping.getId());
        g.setCreated(grouping.getCreated());
        g.setUpdated(grouping.getUpdated());
        g.setDeleted(grouping.getDeleted());
        g.setName(grouping.getName());
        calculateBalance(grouping, g);
        return g;
    }

    private void calculateBalance(Grouping grouping, MGrouping g) {
        List<MGroupingBalance> balance = groupingElementRepository.balance(grouping);
        Map<Long, String> ccyNameDic = new LinkedHashMap<>();
        Map<Long, String> ccySymbDic = new LinkedHashMap<>();
        Map<Long, List<MGroupingBalance>> groupedByCcy = new LinkedHashMap<>();
        Map<Long, BigDecimal> totalByCcy = new LinkedHashMap<>();
        balance.forEach(b -> {
            b.setAmount(MathUtils.round(b.getAmount()));
            if (!groupedByCcy.containsKey(b.getCurrencyId())) {
                groupedByCcy.put(b.getCurrencyId(), new ArrayList<>());
            }
            groupedByCcy.get(b.getCurrencyId()).add(b);
            ccyNameDic.put(b.getCurrencyId(), b.getCurrencyName());
            ccySymbDic.put(b.getCurrencyId(), b.getCurrencySymbol());
            if (!totalByCcy.containsKey(b.getCurrencyId())) {
                totalByCcy.put(b.getCurrencyId(), b.getAmount());
            } else {
                BigDecimal ttl = totalByCcy.get(b.getCurrencyId());
                ttl = MathUtils.round(ttl.add(b.getAmount()));
                totalByCcy.put(b.getCurrencyId(), ttl);
            }
        });
        List<MGrouping.Balance> balances = new ArrayList<>();
        groupedByCcy.forEach((ccyId, gBalances) -> {
            MGrouping.Balance gb = new MGrouping.Balance();
            gb.setCurrencyId(ccyId);
            gb.setCurrencyName(ccyNameDic.get(ccyId));
            gb.setCurrencySymbol(ccySymbDic.get(ccyId));
            gb.setTotal(totalByCcy.get(ccyId));
            Map<String, BigDecimal> byAccount = new LinkedHashMap<>();
            gBalances.forEach(b -> {
                byAccount.put(b.getAccountName(), b.getAmount());
            });
            gb.setByAccount(byAccount);
            balances.add(gb);
        });
        g.setBalances(balances);
    }

}
