package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MUser;
import com.fsquiroz.movements.entity.db.User;
import org.springframework.stereotype.Service;

@Service
public class UserMapper extends Mapper<User, MUser> {

    @Override
    public MUser map(User user) {
        MUser u = new MUser();
        u.setId(user.getId());
        u.setCreated(user.getCreated());
        u.setUpdated(user.getUpdated());
        u.setDeleted(user.getDeleted());
        u.setName(user.getName());
        return u;
    }

}
