package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MLoggedIn;
import com.fsquiroz.movements.entity.api.MUser;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import com.fsquiroz.movements.security.SecurityCheck;
import org.springframework.stereotype.Service;

@Service
public class CredentialMapper extends Mapper<MCredential, MLoggedIn> {

    private UserMapper userMapper;

    public CredentialMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public MLoggedIn map(MCredential cred) {
        MUser movementUser = userMapper.map(SecurityCheck.getAuthenticated().getUser());
        return MLoggedIn.builder()
                .id(cred.getId())
                .created(cred.getCreated())
                .user(cred.getUser())
                .movementUser(movementUser)
                .issuedAt(cred.getIssuedAt())
                .expiration(cred.getExpiration())
                .notBefore(cred.getNotBefore())
                .token(cred.getToken())
                .build();
    }

}
