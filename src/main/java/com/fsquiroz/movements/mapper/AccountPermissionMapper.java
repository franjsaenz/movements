package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MAccountPermission;
import com.fsquiroz.movements.entity.api.MUser;
import com.fsquiroz.movements.entity.db.AccountPermission;
import org.springframework.stereotype.Service;

@Service
public class AccountPermissionMapper extends Mapper<AccountPermission, MAccountPermission> {

    private UserMapper userMapper;

    public AccountPermissionMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public MAccountPermission map(AccountPermission accountPermission) {
        MUser u = userMapper.map(accountPermission.getId().getUser());
        MAccountPermission ap = new MAccountPermission();
        ap.setUser(u);
        ap.setPermission(accountPermission.getPermission());
        return ap;
    }

}
