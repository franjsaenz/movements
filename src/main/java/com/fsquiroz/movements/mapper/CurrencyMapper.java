package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MCurrency;
import com.fsquiroz.movements.entity.db.Currency;
import org.springframework.stereotype.Service;

@Service
public class CurrencyMapper extends Mapper<Currency, MCurrency> {

    @Override
    public MCurrency map(Currency currency) {
        MCurrency c = new MCurrency();
        c.setId(currency.getId());
        c.setIsoCode(currency.getIsoCode());
        c.setSymbol(currency.getSymbol());
        c.setName(currency.getName());
        return c;
    }

}
