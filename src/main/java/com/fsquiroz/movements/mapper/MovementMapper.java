package com.fsquiroz.movements.mapper;

import com.fsquiroz.movements.entity.api.MMovement;
import com.fsquiroz.movements.entity.db.Movement;
import org.springframework.stereotype.Service;

@Service
public class MovementMapper extends Mapper<Movement, MMovement> {

    private UserMapper userMapper;

    public MovementMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public MMovement map(Movement movement) {
        MMovement m = new MMovement();
        m.setId(movement.getId());
        m.setCreated(movement.getCreated());
        m.setCreator(userMapper.map(movement.getOwner()));
        m.setUpdated(movement.getUpdated());
        if (movement.getEditor() != null) {
            m.setUpdater(userMapper.map(movement.getEditor()));
        }
        m.setDeleted(movement.getDeleted());
        m.setRegistered(movement.getRegistered());
        m.setDescription(movement.getDescription());
        m.setCredit(movement.getCredit());
        m.setDebit(movement.getDebit());
        return m;
    }

}
