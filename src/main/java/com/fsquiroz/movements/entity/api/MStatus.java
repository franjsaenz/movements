package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Status")
public class MStatus {

    @ApiModelProperty(example = "Expenses")
    private String name;

    @ApiModelProperty(example = "1.0.1")
    private String version;

    @ApiModelProperty(example = "2019-01-25T12:57:01.999Z")
    private Instant build;

    @ApiModelProperty(example = "2019-01-25T12:57:01.999Z")
    private Instant startUp;

    @ApiModelProperty(example = "Up")
    private String message;

}
