package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GroupingBalance")
public class MGroupingBalance {

    private Long accountId;

    private String accountName;

    private Long currencyId;

    private String currencyName;

    private String currencySymbol;

    private BigDecimal amount;

}
