package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CreateUser")
public class MCreateUser {

    @ApiModelProperty(example = "some@email.com")
    private String email;

    @ApiModelProperty(example = "somePass")
    private String password;

    @ApiModelProperty(example = "Name")
    private String name;

}
