package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ChangePassword")
public class MChangePassword {

    @ApiModelProperty(example = "oldPassword")
    private String oldPassword;

    @ApiModelProperty(example = "newPassword")
    private String newPassword;

}
