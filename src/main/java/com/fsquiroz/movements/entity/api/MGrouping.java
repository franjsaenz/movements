package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Grouping")
public class MGrouping {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "Account name")
    private String name;

    @ApiModelProperty(hidden = true)
    private List<Balance> balances;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Balance {

        private Long currencyId;

        private String currencyName;

        private String currencySymbol;

        private Map<String, BigDecimal> byAccount;

        private BigDecimal total;

    }
}
