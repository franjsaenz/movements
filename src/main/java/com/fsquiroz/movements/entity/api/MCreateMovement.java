package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CreateMovement")
public class MCreateMovement {

    @ApiModelProperty(example = "20201224T21:00:00.000Z")
    protected Instant registered;

    @ApiModelProperty(example = "Withdraw from ATM")
    private String description;

    @ApiModelProperty(example = "200")
    private BigDecimal amount;

    @ApiModelProperty(example = "true")
    private Boolean debit;

}
