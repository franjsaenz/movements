package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Response")
public class MResponse {

    @ApiModelProperty(example = "2019-01-25T12:57:01.999Z")
    private Instant timestamp;

    @ApiModelProperty(example = "Some message")
    private String message;

    public static MResponse of(String message) {
        Instant now = Instant.now();
        return new MResponse(now, message);
    }

}
