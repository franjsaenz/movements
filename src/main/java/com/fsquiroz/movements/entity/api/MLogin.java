package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "password")
public class MLogin {

    @ApiModelProperty(example = "some@email.com")
    private String platformIdentifier;

    @ApiModelProperty(example = "some-password123")
    private String password;

}
