package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Account")
public class MAccount {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(hidden = true)
    private MCurrency currency;

    @ApiModelProperty(example = "Account name")
    private String name;

    @ApiModelProperty(example = "true")
    private boolean canEdit;

    @ApiModelProperty(example = "true")
    private boolean owner;

    private MUser creator;

    @ApiModelProperty(example = "5105.75")
    private BigDecimal balance;

}
