package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Movement")
public class MMovement {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    private MUser creator;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    private MUser updater;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(hidden = true)
    protected Instant registered;

    @ApiModelProperty(example = "Withdraw from ATM")
    private String description;

    @ApiModelProperty(example = "200")
    private BigDecimal credit;

    @ApiModelProperty(example = "200")
    private BigDecimal debit;

}
