package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "LoggedIn")
public class MLoggedIn {

    private String id;

    private Instant created;

    private com.fsquiroz.movements.integration.fsauth.api.MUser user;

    private MUser movementUser;

    private Instant issuedAt;

    private Instant expiration;

    private Instant notBefore;

    private String token;

}
