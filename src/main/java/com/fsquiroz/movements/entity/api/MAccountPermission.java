package com.fsquiroz.movements.entity.api;

import com.fsquiroz.movements.entity.db.Permission;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AccountPermission")
public class MAccountPermission {

    @ApiModelProperty(hidden = true)
    private MUser user;

    @ApiModelProperty(hidden = true)
    private Permission permission;

}
