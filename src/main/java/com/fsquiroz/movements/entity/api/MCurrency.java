package com.fsquiroz.movements.entity.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Currency")
public class MCurrency {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "USD")
    private String isoCode;

    @ApiModelProperty(example = "$")
    private String symbol;

    @ApiModelProperty(example = "US Dollar")
    private String name;

}
