package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@ToString(callSuper = true)
@MappedSuperclass
public abstract class Filterable extends Entity {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    protected User owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    protected User editor;

}
