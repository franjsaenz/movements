package com.fsquiroz.movements.entity.db;

import lombok.Getter;

@Getter
public enum Permission {

    READ("READ"),
    WRITE("WRITE");

    private String name;

    Permission(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
