package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class GroupingElement extends Filterable {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Account account;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Grouping grouping;

}
