package com.fsquiroz.movements.entity.db;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@javax.persistence.Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
public class AccountPermission {

    @EmbeddedId
    private PermissionId id;

    @Enumerated(EnumType.STRING)
    private Permission permission;

    @Embeddable
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode(of = {"account", "user"})
    public static class PermissionId implements Serializable {

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn
        private Account account;

        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn
        private User user;

    }

}
