package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Grouping extends Filterable {

    @Column(columnDefinition = "varchar(100) NOT NULL")
    private String name;

    @OneToMany(mappedBy = "grouping")
    @Column(updatable = false, insertable = false)
    private List<GroupingElement> elements;

}
