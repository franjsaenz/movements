package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Account extends Filterable {

    @Column(columnDefinition = "varchar(100) NOT NULL")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Currency currency;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id.account", fetch = FetchType.EAGER)
    private Set<AccountPermission> permissions = new LinkedHashSet<>();

}
