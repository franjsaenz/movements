package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class User extends Entity {

    @Column(columnDefinition = "varchar(100) NOT NULL")
    private String name;

    @Column(columnDefinition = "varchar(100) NOT NULL")
    private String email;

    @Column(columnDefinition = "varchar(255) NOT NULL", unique = true)
    private String authId;

}
