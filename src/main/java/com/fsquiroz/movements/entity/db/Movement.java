package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.Instant;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Movement extends Filterable {

    @Column(columnDefinition = "TIMESTAMP DEFAULT NOW()")
    protected Instant registered;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private Account account;

    @Column(columnDefinition = "varchar(100) NULL")
    private String description;

    @Column(scale = 2)
    private BigDecimal credit;

    @Column(scale = 2)
    private BigDecimal debit;

}
