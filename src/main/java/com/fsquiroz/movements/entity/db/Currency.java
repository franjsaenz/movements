package com.fsquiroz.movements.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Currency extends Entity {

    @Column(columnDefinition = "varchar(3) NOT NULL")
    private String isoCode;

    @Column(columnDefinition = "varchar(100) NOT NULL")
    private String name;

    @Column(columnDefinition = "varchar(5) DEFAULT '$'")
    private String symbol;

}
