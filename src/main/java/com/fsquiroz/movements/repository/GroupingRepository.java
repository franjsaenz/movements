package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupingRepository extends JpaRepository<Grouping, Long> {

    @Query("SELECT g FROM Grouping g WHERE g.deleted IS NULL AND g.owner = :user")
    Page<Grouping> findAllByOwner(@Param("user") User user, Pageable page);

    @Query("SELECT g FROM Grouping g WHERE g.deleted IS NULL AND g.owner = :user AND g.name LIKE %:term%")
    Page<Grouping> searchAllByOwner(@Param("user") User user, @Param("term") String term, Pageable page);

}
