package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Movement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementRepository extends JpaRepository<Movement, Long> {

    @Query("SELECT m FROM Movement m WHERE m.deleted IS NULL AND m.account = :account")
    Page<Movement> search(Account account, Pageable page);

    @Query("SELECT m FROM Movement m WHERE m.deleted IS NULL AND m.account = :account AND m.description LIKE %:term%")
    Page<Movement> search(@Param("account") Account account, @Param("term") String term, Pageable page);

}
