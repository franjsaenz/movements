package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByAuthId(String authId);

    @Query("SELECT u FROM User u WHERE u.deleted IS NULL AND u.name LIKE %:term%")
    Page<User> search(@Param("term") String term, Pageable page);

    @Query("SELECT u FROM User u WHERE u.deleted IS NULL")
    Page<User> search(Pageable page);

}
