package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.api.MGroupingBalance;
import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.entity.db.GroupingElement;
import com.fsquiroz.movements.entity.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupingElementRepository extends JpaRepository<GroupingElement, Long> {

    Optional<GroupingElement> findByGroupingAndAccount(Grouping grouping, Account account);

    @Query("SELECT new com.fsquiroz.movements.entity.api.MGroupingBalance(a.id, a.name, c.id, c.name, c.symbol, COALESCE(SUM(m.credit), 0) - COALESCE(SUM(m.debit), 0)) FROM GroupingElement e JOIN e.account a JOIN a.currency c JOIN Movement  m ON m.account = a WHERE e.grouping = :grouping GROUP BY a ORDER BY a.id")
    List<MGroupingBalance> balance(@Param("grouping") Grouping grouping);

    @Modifying
    @Transactional
    @Query("DELETE FROM GroupingElement e WHERE e.id = :id")
    void delete(@Param("id") Long id);

    @Modifying
    @Transactional
    @Query("DELETE FROM GroupingElement e WHERE e.account = :account")
    void delete(@Param("account") Account account);

    @Modifying
    @Transactional
    @Query("DELETE FROM GroupingElement e WHERE e.account = :account AND e.owner = :user")
    void delete(@Param("account") Account account, @Param("user") User user);

}
