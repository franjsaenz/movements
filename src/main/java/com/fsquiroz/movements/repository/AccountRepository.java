package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.Grouping;
import com.fsquiroz.movements.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT e.account FROM GroupingElement e WHERE e.grouping = :grouping")
    Page<Account> findByGrouping(@Param("grouping") Grouping grouping, Pageable page);

    @Query("SELECT DISTINCT a FROM Account a LEFT JOIN a.permissions ap WHERE a.deleted IS NULL AND (a.owner = :user OR ap.id.user = :user)")
    Page<Account> search(@Param("user") User user, Pageable page);

    @Query("SELECT DISTINCT a FROM Account a LEFT JOIN a.permissions ap WHERE a.deleted IS NULL AND ((a.owner = :user OR ap.id.user = :user) AND a.name LIKE %:term%)")
    Page<Account> search(@Param("user") User user, @Param("term") String term, Pageable page);

    @Query("SELECT COALESCE(SUM(m.credit), 0) - COALESCE(SUM(m.debit), 0) FROM Movement m JOIN m.account a WHERE m.deleted IS NULL AND m.account = :account")
    BigDecimal getTotal(@Param("account") Account account);

}
