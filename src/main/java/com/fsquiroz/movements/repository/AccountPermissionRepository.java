package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.Account;
import com.fsquiroz.movements.entity.db.AccountPermission;
import com.fsquiroz.movements.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface AccountPermissionRepository extends JpaRepository<AccountPermission, Long> {

    @Query("SELECT ap FROM AccountPermission ap WHERE ap.id.account = :account")
    Page<AccountPermission> findByAccount(@Param("account") Account account, Pageable page);

    Optional<AccountPermission> findById(AccountPermission.PermissionId id);

    boolean existsById(AccountPermission.PermissionId id);

    @Modifying
    @Transactional
    @Query("DELETE FROM AccountPermission ap WHERE ap.id.account = :account AND ap.id.user = :user")
    void delete(
            @Param("account") Account account,
            @Param("user") User user
    );

}
