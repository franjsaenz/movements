package com.fsquiroz.movements.repository;

import com.fsquiroz.movements.entity.db.Currency;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    @Query("SELECT c FROM Currency c WHERE c.deleted IS NULL AND (c.name LIKE %:term% OR c.isoCode LIKE %:term%)")
    Page<Currency> search(@Param("term") String term, Pageable page);

    @Query("SELECT c FROM Currency c WHERE c.deleted IS NULL")
    Page<Currency> search(Pageable page);

}
