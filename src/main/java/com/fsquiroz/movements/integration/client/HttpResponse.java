package com.fsquiroz.movements.integration.client;

import lombok.ToString;

import java.io.IOException;

public interface HttpResponse<RESPONSE, ERROR> {

    /**
     * Returns the original Request
     *
     * @return the original Request
     */
    HttpRequest<RESPONSE, ERROR> getRequest();

    /**
     * Returns the Ok Response
     *
     * @return the Ok Response
     */
    RESPONSE getResponse();

    /**
     * Returns the Error Response
     *
     * @return the Error Response
     */
    ERROR getError();

    /**
     * Returns the Http Status Code
     *
     * @return the Http Status Code
     */
    int getStatus();

    /**
     * Returns the Exception if one occurred
     *
     * @return the Exception if one occurred
     */
    IOException getException();

    /**
     * Returns the total duration of the HTTP call
     *
     * @return the total duration of the HTTP call
     */
    long getDuration();

    /**
     * Checks if got an Ok response
     *
     * @return true if getRequest() returns a non null value
     */
    default boolean isOk() {
        return getRequest() != null;
    }

    /**
     * Checks if got an Error response
     *
     * @return true if getError() returns a non null value
     */
    default boolean isError() {
        return getError() != null;
    }

    /**
     * Checks if call could not execute successfully
     *
     * @return true if getException() returns a non null value
     */
    default boolean isException() {
        return getException() != null;
    }

    @ToString
    class Implementation<RESPONSE, ERROR> implements HttpResponse<RESPONSE, ERROR> {

        private HttpRequest<RESPONSE, ERROR> request;

        private RESPONSE response;

        private ERROR error;

        private int status;

        private IOException exception;

        private long duration;

        public Implementation(HttpRequest<RESPONSE, ERROR> request, RESPONSE response, ERROR error, int status, IOException exception, long duration) {
            this.request = request;
            this.response = response;
            this.error = error;
            this.status = status;
            this.exception = exception;
            this.duration = duration;
        }

        public HttpRequest<RESPONSE, ERROR> getRequest() {
            return request;
        }

        public RESPONSE getResponse() {
            return response;
        }

        public ERROR getError() {
            return error;
        }

        public int getStatus() {
            return status;
        }

        public IOException getException() {
            return exception;
        }

        public long getDuration() {
            return duration;
        }

    }

}
