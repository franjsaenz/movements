package com.fsquiroz.movements.integration.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.ToString;
import okhttp3.MediaType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@ToString(exclude = {"responseType", "errorType", "mapper"})
public class HttpRequest<RESPONSE, ERROR> {

    private Action action;

    private String url;

    private Map<String, List<String>> parameters;

    private Map<String, List<String>> headers;

    private Object body;

    private MediaType mediaType;

    private ObjectMapper mapper;

    private TypeReference<RESPONSE> responseType;

    private TypeReference<ERROR> errorType;

    HttpRequest(Map<String, List<String>> parameters, Map<String, List<String>> headers, Object body, MediaType mediaType, TypeReference<RESPONSE> responseType, TypeReference<ERROR> errorType) {
        this.parameters = parameters;
        this.headers = headers;
        this.body = body;
        this.mediaType = mediaType;
        this.responseType = responseType;
        this.errorType = errorType;
    }

    void setAction(Action action) {
        this.action = action;
    }

    void setUrl(String url) {
        this.url = url;
    }

    void setBody(Object body) {
        this.body = body;
    }

    void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    void setResponseType(TypeReference<RESPONSE> responseType) {
        this.responseType = responseType;
    }

    void setErrorType(TypeReference<ERROR> errorType) {
        this.errorType = errorType;
    }

    Action getAction() {
        return action;
    }

    String getUrl() {
        return url;
    }

    Map<String, List<String>> getParameters() {
        return parameters;
    }

    Map<String, List<String>> getHeaders() {
        return headers;
    }

    Object getBody() {
        return body;
    }

    MediaType getMediaType() {
        return mediaType;
    }

    ObjectMapper getMapper() {
        return mapper;
    }

    TypeReference<RESPONSE> getResponseType() {
        return responseType;
    }

    TypeReference<ERROR> getErrorType() {
        return errorType;
    }

    public enum Action {
        GET, POST, PUT, DELETE
    }

    public static class Builder<RESPONSE, ERROR> {

        private HttpRequest<RESPONSE, ERROR> request;

        private List<String> urlBuilder;

        private boolean built;

        public Builder() {
            request = new HttpRequest<>(
                    new TreeMap<>(),
                    new TreeMap<>(),
                    null,
                    null,
                    new TypeReference<RESPONSE>() {
                    },
                    new TypeReference<ERROR>() {
                    }
            );
            urlBuilder = new ArrayList<>();
        }

        public Builder<RESPONSE, ERROR> addParam(String key, String value) {
            notBuilt();
            addToMap(request.getParameters(), key, value);
            return this;
        }

        public Builder<RESPONSE, ERROR> addHeader(String key, String value) {
            notBuilt();
            addToMap(request.getHeaders(), key, value);
            return this;
        }

        public Builder<RESPONSE, ERROR> setBody(Object body) {
            notBuilt();
            request.setBody(body);
            return this;
        }

        public Builder<RESPONSE, ERROR> setBody(Object body, MediaType mediaType) {
            notBuilt();
            request.setBody(body);
            request.setMediaType(mediaType);
            return this;
        }

        public Builder<RESPONSE, ERROR> setMapper(ObjectMapper mapper) {
            notBuilt();
            request.setMapper(mapper);
            return this;
        }

        public Builder<RESPONSE, ERROR> setResponseType(TypeReference<RESPONSE> typeReference) {
            notBuilt();
            request.setResponseType(typeReference);
            return this;
        }

        public Builder<RESPONSE, ERROR> setErrorType(TypeReference<ERROR> typeReference) {
            notBuilt();
            request.setErrorType(typeReference);
            return this;
        }

        public Builder<RESPONSE, ERROR> addToUrl(String val) {
            urlBuilder.add(val);
            return this;
        }

        public HttpRequest<RESPONSE, ERROR> build(Action action) {
            if (action == null) {
                throw new IllegalArgumentException("Request can not be built with a null action");
            }
            try {
                request.setAction(action);
                request.setUrl(String.join("", urlBuilder));
                return request;
            } finally {
                built = true;
            }
        }

        private void addToMap(Map<String, List<String>> map, String key, String value) {
            if (map.containsKey(key)) {
                map.get(key).add(value);
            } else {
                List<String> values = new ArrayList<>();
                values.add(value);
                map.put(key, values);
            }
        }

        private void notBuilt() {
            if (built) {
                throw new UnsupportedOperationException("An already built builder can not be modified");
            }
        }

    }

}
