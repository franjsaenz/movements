package com.fsquiroz.movements.integration.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class HttpClient {

    private OkHttpClient client;

    private ObjectMapper mapper;

    public HttpClient(ObjectMapper mapper) {
        this.mapper = mapper;
        client = new OkHttpClient.Builder()
                .build();
    }

    public <RESPONSE, ERROR> HttpResponse<RESPONSE, ERROR> execute(HttpRequest<RESPONSE, ERROR> request) {
        log.trace("Preparing to perform HTTP call with request: {}", request);
        HttpResponse<RESPONSE, ERROR> httpResponse;
        long now = System.currentTimeMillis();
        try {
            ObjectMapper mapper = getMapper(request);
            Request.Builder requestBuilder = buildRequest(request);
            if (HttpRequest.Action.GET.equals(request.getAction())) {
                requestBuilder.get();
            } else if (HttpRequest.Action.DELETE.equals(request.getAction())) {
                requestBuilder.delete();
            } else {
                RequestBody body;
                if (request.getBody() == null) {
                    body = RequestBody.create("", MediaType.get("text/plain"));
                } else {
                    MediaType mediaType = request.getMediaType() != null ? request.getMediaType() : MediaType.get("application/json; charset=utf-8");
                    body = RequestBody.create(mapper.writeValueAsString(request.getBody()), mediaType);
                }
                switch (request.getAction()) {
                    case POST:
                        requestBuilder.post(body);
                        break;
                    case PUT:
                        requestBuilder.put(body);
                        break;
                }
            }
            now = System.currentTimeMillis();
            Response resp = client.newCall(requestBuilder.build()).execute();
            if (resp.isSuccessful()) {
                RESPONSE response = mapper.readValue(resp.body().byteStream(), request.getResponseType());
                httpResponse = new HttpResponse.Implementation<>(request, response, null, resp.code(), null, System.currentTimeMillis() - now);
            } else {
                ERROR error = mapper.readValue(resp.body().byteStream(), request.getErrorType());
                httpResponse = new HttpResponse.Implementation<>(request, null, error, resp.code(), null, System.currentTimeMillis() - now);
            }
            log.trace("HTTP call response: {}", httpResponse);
        } catch (IOException e) {
            httpResponse = new HttpResponse.Implementation<>(request, null, null, -1, e, System.currentTimeMillis() - now);
            log.error("Unable to perform HTTP call. Caused by: {}", e.getMessage(), e);
        }
        log.debug("Performed HTTP call:\nReq ->  {}\nResp -> {}", request, httpResponse);
        return httpResponse;
    }

    private Request.Builder buildRequest(HttpRequest<?, ?> request) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(request.getUrl()).newBuilder();
        request.getParameters().forEach((k, vs) -> vs.forEach(v -> urlBuilder.addQueryParameter(k, v)));
        Request.Builder req = new Request.Builder()
                .url(urlBuilder.build());
        request.getHeaders().forEach((k, vs) -> vs.forEach(v -> req.addHeader(k, v)));
        return req;
    }

    private ObjectMapper getMapper(HttpRequest<?, ?> request) {
        return request.getMapper() != null ? request.getMapper() : this.mapper;
    }

}
