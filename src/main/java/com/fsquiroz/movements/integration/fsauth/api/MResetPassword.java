package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MResetPassword {

    private String oldPassword;

    private String newPassword;

    private String token;

}
