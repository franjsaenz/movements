package com.fsquiroz.movements.integration.fsauth.api;

public enum Role {

    SUPER("SUPER"),
    PLATFORM_ADMIN("PLATFORM_ADMIN"),
    PLATFORM_USER("PLATFORM_USER"),
    PLATFORM_CLIENT("PLATFORM_CLIENT");

    private String name;

    Role(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
