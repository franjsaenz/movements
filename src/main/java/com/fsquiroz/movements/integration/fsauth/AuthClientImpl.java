package com.fsquiroz.movements.integration.fsauth;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fsquiroz.movements.integration.client.HttpClient;
import com.fsquiroz.movements.integration.client.HttpRequest;
import com.fsquiroz.movements.integration.client.HttpResponse;
import com.fsquiroz.movements.integration.fsauth.api.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AuthClientImpl implements AuthClient {

    private static final String LOGIN_ENDPOINT = "/login";

    private static final String USERS_ENDPOINT = "/users";

    private static final String PASSWORD_ENDPOINT = "/password";

    private String rootUrl;

    private String clientToken;

    private HttpClient client;

    public AuthClientImpl(
            @Value("${com.fsquiroz.auth.root-url}") String rootUrl,
            @Value("${com.fsquiroz.auth.token}") String clientToken,
            HttpClient client
    ) {
        this.rootUrl = rootUrl;
        this.clientToken = clientToken;
        this.client = client;
    }

    public MCredential getLoggedIn(MCredential originalCredential) {
        HttpRequest.Builder<MCredential, MException> request = new HttpRequest.Builder<MCredential, MException>()
                .setResponseType(new TypeReference<MCredential>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(LOGIN_ENDPOINT);
        addToken(request, originalCredential);
        return call(request.build(HttpRequest.Action.GET));
    }

    public MCredential login(MLogin login) {
        HttpRequest.Builder<MCredential, MException> request = new HttpRequest.Builder<MCredential, MException>()
                .setResponseType(new TypeReference<MCredential>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(LOGIN_ENDPOINT)
                .setBody(login);
        addToken(request);
        return call(request.build(HttpRequest.Action.POST));
    }

    public MUser getUser(String userId, MCredential originalCredential) {
        HttpRequest.Builder<MUser, MException> request = new HttpRequest.Builder<MUser, MException>()
                .setResponseType(new TypeReference<MUser>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl("/")
                .addToUrl(userId);
        addToken(request, originalCredential);
        return call(request.build(HttpRequest.Action.GET));
    }

    public MUser createUser(MUser toCreate) {
        HttpRequest.Builder<MUser, MException> request = new HttpRequest.Builder<MUser, MException>()
                .setResponseType(new TypeReference<MUser>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .setBody(toCreate);
        addToken(request);
        return call(request.build(HttpRequest.Action.POST));
    }

    public MUser editUser(String userId, MUser edition, MCredential originalCredential) {
        HttpRequest.Builder<MUser, MException> request = new HttpRequest.Builder<MUser, MException>()
                .setResponseType(new TypeReference<MUser>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl("/")
                .addToUrl(userId)
                .setBody(edition);
        addToken(request, originalCredential);
        return call(request.build(HttpRequest.Action.PUT));
    }

    public void deleteUser(String userId, MCredential originalCredential) {
        HttpRequest.Builder<MResponse, MException> request = new HttpRequest.Builder<MResponse, MException>()
                .setResponseType(new TypeReference<MResponse>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl("/")
                .addToUrl(userId);
        addToken(request, originalCredential);
        call(request.build(HttpRequest.Action.DELETE));
    }

    public void updatePassword(String userId, MResetPassword resetPassword, MCredential originalCredential) {
        HttpRequest.Builder<MResponse, MException> request = new HttpRequest.Builder<MResponse, MException>()
                .setResponseType(new TypeReference<MResponse>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl("/")
                .addToUrl(userId)
                .addToUrl("/password")
                .setBody(resetPassword);
        addToken(request, originalCredential);
        call(request.build(HttpRequest.Action.POST));
    }

    public MCredential resetPassword(String platformIdentifier) {
        HttpRequest.Builder<MCredential, MException> request = new HttpRequest.Builder<MCredential, MException>()
                .setResponseType(new TypeReference<MCredential>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl(PASSWORD_ENDPOINT)
                .addParam("platformIdentifier", platformIdentifier);
        addToken(request);
        return call(request.build(HttpRequest.Action.GET));
    }

    public void resetPassword(String platformIdentifier, MResetPassword resetPassword) {
        HttpRequest.Builder<MResponse, MException> request = new HttpRequest.Builder<MResponse, MException>()
                .setResponseType(new TypeReference<MResponse>() {
                })
                .setErrorType(new TypeReference<MException>() {
                })
                .addToUrl(rootUrl)
                .addToUrl(USERS_ENDPOINT)
                .addToUrl(PASSWORD_ENDPOINT)
                .addParam("platformIdentifier", platformIdentifier)
                .setBody(resetPassword);
        addToken(request);
        call(request.build(HttpRequest.Action.POST));
    }

    private void addToken(HttpRequest.Builder<?, ?> request, MCredential originalCredential) {
        if (originalCredential != null) {
            request.addHeader("Authorization", originalCredential.getToken());
        }
    }

    private void addToken(HttpRequest.Builder<?, ?> request) {
        request.addHeader("Authorization", clientToken);
    }

    private <R> R call(HttpRequest<R, MException> request) {
        HttpResponse<R, MException> response = client.execute(request);
        throwError(response);
        return response.getResponse();
    }

    private void throwError(HttpResponse<?, MException> response) {
        if (response.getError() != null) {
            throw new AuthException(response.getError());
        }
        if (response.getException() != null) {
            throw new RuntimeException("There has been an communication error while trying to call Auth Service", response.getException());
        }
    }

}
