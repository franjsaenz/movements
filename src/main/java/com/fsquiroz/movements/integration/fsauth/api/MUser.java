package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "password")
public class MUser {

    private String id;

    private Instant created;

    private Instant updated;

    private Instant deleted;

    private Role role;

    private String platformIdentifier;

    private String password;

    private Map<String, Object> attributes;

    private MPlatform platform;

}
