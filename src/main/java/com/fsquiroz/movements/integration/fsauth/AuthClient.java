package com.fsquiroz.movements.integration.fsauth;

import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import com.fsquiroz.movements.integration.fsauth.api.MCredential;
import com.fsquiroz.movements.integration.fsauth.api.MLogin;
import com.fsquiroz.movements.integration.fsauth.api.MResetPassword;
import com.fsquiroz.movements.integration.fsauth.api.MUser;

public interface AuthClient {

    /**
     * Get credential details for current logged in user. This method does not provides a Bearer Token
     *
     * @param originalCredential credential for current logged in user
     * @return the credential details for current logged in user without Bearer Token
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MCredential getLoggedIn(MCredential originalCredential);

    /**
     * Login an user with the provided platform identifier and password. This method does provides the
     * Bearer Token to user for authentication
     *
     * @param login entity with platform identifier and password
     * @return the credential details for logged in user with Bearer Token to user for authentication
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MCredential login(MLogin login);

    /**
     * Get user information for the provided user id
     *
     * @param userId             id of user to fetch
     * @param originalCredential credential for current logged in user
     * @return the user information
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MUser getUser(String userId, MCredential originalCredential);

    /**
     * Create an user in the Auth Service
     *
     * @param toCreate information for new user
     * @return the created user information
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MUser createUser(MUser toCreate);

    /**
     * Edit user information in the Auth Service
     *
     * @param userId             id of user to edit
     * @param edition            new information for the user
     * @param originalCredential credential for current logged in user
     * @return the edited user information
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MUser editUser(String userId, MUser edition, MCredential originalCredential);

    /**
     * Delete user from Auth Service
     *
     * @param userId             id of user to delete
     * @param originalCredential credential for current logged in user
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    void deleteUser(String userId, MCredential originalCredential);

    /**
     * Update user's password providing old and new one
     *
     * @param userId             id of user to update password
     * @param resetPassword      old and new password
     * @param originalCredential credential for current logged in user
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    void updatePassword(String userId, MResetPassword resetPassword, MCredential originalCredential);

    /**
     * Start the process to reset an user's password. THis method returns a credential with a token than
     * must be send to the user though a secured and confirmed email, phone or other external communication
     * mean
     *
     * @param platformIdentifier user's platform identifier to reset password
     * @return the secret token to confirm user's identification. This token must be send to the user though
     * a secured and confirmed email, phone or other external communication mean
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    MCredential resetPassword(String platformIdentifier);

    /**
     * Override old user's password with the provided new one. This method must contains the token generated
     * in the first step to authenticate the user's identity
     *
     * @param platformIdentifier user's platform identifier to reset password
     * @param resetPassword      new password and secret token
     * @throws RuntimeException when there has been a communication error during the http call, or if
     *                          fsAuth responds with 500 status code
     * @throws AuthException    when fsAuth responds with 4xx status code
     */
    void resetPassword(String platformIdentifier, MResetPassword resetPassword);

}
