package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MPlatform {

    private String id;

    private Instant created;

    private Instant updated;

    private Instant deleted;

    private String name;

    private String description;

}
