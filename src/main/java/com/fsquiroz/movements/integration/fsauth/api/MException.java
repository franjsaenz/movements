package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MException {

    private Instant timestamp;

    private int status;

    private String error;

    private String message;

    private String path;

    private Map<String, Object> meta;

    private String errorCode;

}
