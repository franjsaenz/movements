package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "password")
public class MLogin {

    private String platformIdentifier;

    private String password;

    private String token;

}
