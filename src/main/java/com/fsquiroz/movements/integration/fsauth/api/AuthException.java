package com.fsquiroz.movements.integration.fsauth.api;

import lombok.Getter;
import lombok.ToString;

import java.time.Instant;
import java.util.Map;

@Getter
@ToString
public class AuthException extends RuntimeException {

    private Instant timestamp;

    private int status;

    private String error;

    private String message;

    private String path;

    private Map<String, Object> meta;

    private String errorCode;

    public AuthException(MException e) {
        timestamp = e.getTimestamp();
        status = e.getStatus();
        error = e.getError();
        message = e.getMessage();
        path = e.getPath();
        meta = e.getMeta();
        errorCode = e.getErrorCode();
    }

}
