package com.fsquiroz.movements.integration.fsauth.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "token")
public class MCredential {

    private String id;

    private Instant created;

    private Instant updated;

    private Instant deleted;

    private MUser user;

    private MUser issuer;

    private Instant issuedAt;

    private Instant expiration;

    private Instant notBefore;

    private String token;

}
