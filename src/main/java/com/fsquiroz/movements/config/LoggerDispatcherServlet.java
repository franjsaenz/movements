package com.fsquiroz.movements.config;

import com.fsquiroz.movements.security.Authenticated;
import com.fsquiroz.movements.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class LoggerDispatcherServlet extends DispatcherServlet {

    private SecurityCheck securityCheck;

    public LoggerDispatcherServlet(SecurityCheck securityCheck) {
        this.securityCheck = securityCheck;
    }

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long start = System.currentTimeMillis();
        HandlerExecutionChain handler = getHandler(request);
        try {
            super.doDispatch(request, response);
        } finally {
            if (handler.toString().contains("fsquiroz")) {
                long end = System.currentTimeMillis();
                debug(request, response, handler, end - start);
            }
        }
    }

    private void debug(HttpServletRequest request,
                       HttpServletResponse response,
                       HandlerExecutionChain handler,
                       long elapsed) {
        if (log.isInfoEnabled()) {
            Authenticated a;
            try {
                a = securityCheck.getAuthentication();
            } catch (Exception e) {
                a = null;
            }
            String user = a != null ? a.getUsername() : "ANONYMOUS";
            String role =
                    a != null && a.getCredential() != null && a.getCredential().getUser() != null &&
                            a.getCredential().getUser().getRole() != null ?
                            a.getCredential().getUser().getRole().getName() : "ANONYMOUS";
            log.debug("\n"
                            + "HTTP Request:\n"
                            + "\turl: {}\n"
                            + "\tmethod: {}\n"
                            + "\tclient: {}\n"
                            + "\tuserId: {}\n"
                            + "\tuserEmail: {}\n"
                            + "\tuserRole: {}\n"
                            + "Handler:\n"
                            + "\tmethod: {}\n"
                            + "\ttook: {}ms\n"
                            + "HTTP Response:\n"
                            + "\tstatus: {}\n",
                    request.getRequestURL(),
                    request.getMethod(),
                    request.getRemoteAddr(),
                    a != null && a.getCredential() != null && a.getUser() != null && a.getUser().getId() != null ?
                            a.getUser().getId() : "ANONYMOUS",
                    user,
                    role,
                    handler.toString(),
                    elapsed,
                    response.getStatus());
        }
    }

}
