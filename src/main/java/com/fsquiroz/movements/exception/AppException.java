package com.fsquiroz.movements.exception;

import lombok.Getter;

import java.util.Map;

@Getter
public abstract class AppException extends RuntimeException {

    private ErrorCode code;

    private Map<String, Object> meta;

    protected AppException(ErrorCode code, Map<String, Object> meta, String message) {
        super(message);
        this.code = code;
        this.meta = meta;
    }

    protected AppException(ErrorCode code, Map<String, Object> meta, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.meta = meta;
    }

}
