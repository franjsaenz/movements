package com.fsquiroz.movements.exception;

import com.fsquiroz.movements.entity.db.*;
import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends AppException {

    private BadRequestException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static BadRequestException byMissingParam(String param) {
        Assert.hasText(param, "'param' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_MISSING_PARAM, meta, "Missing param");
    }

    public static BadRequestException byEmptyParam(String param) {
        Assert.hasText(param, "'param' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_MISSING_PARAM, meta, "Param can not be empty");
    }

    public static BadRequestException byExceedingSize(String param, int size) {
        Assert.hasText(param, "'param' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("param", param);
        meta.put("maxSize", size);
        return new BadRequestException(ErrorCode.BR_PARAM_TOO_LONG, meta, "Param has value too long");
    }

    public static BadRequestException byInvalidId() {
        return new BadRequestException(ErrorCode.BR_INVALID_ID_VALUE, null, "Invalid id value");
    }

    public static BadRequestException byGrantPermissionToOwner(Account account, User user) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("user", ofUser(user));
        meta.put("account", ofFilterable(account));
        return new BadRequestException(ErrorCode.BR_GRANT_PERMISSION_TO_OWNER, meta, "Can not grant permission to owner");
    }

    public static BadRequestException byEditingDeleted(Entity entity) {
        Map<String, Object> meta = ofEntity(entity);
        meta.put("deleted", entity.getDeleted());
        return new BadRequestException(ErrorCode.BR_EDIT_DELETED, meta, "Can not edit a deleted entity");
    }

    public static BadRequestException byDeletingDeleted(Entity entity) {
        Map<String, Object> meta = ofEntity(entity);
        meta.put("deleted", entity.getDeleted());
        return new BadRequestException(ErrorCode.BR_DELETE_DELETED, meta, "Entity already deleted");
    }

    public static BadRequestException byUsingDeleted(Entity deleted, Entity into) {
        Map<String, Object> d = ofEntity(deleted);
        d.put("deleted", deleted.getDeleted());
        Map<String, Object> i = ofEntity(into);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("deleted", d);
        meta.put("useInto", i);
        return new BadRequestException(ErrorCode.BR_USING_DELETED, meta, "Can not use deleted entity");
    }

    public static BadRequestException byUsingDeleted(Entity deleted) {
        Map<String, Object> d = ofEntity(deleted);
        d.put("deleted", deleted.getDeleted());
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("deleted", d);
        return new BadRequestException(ErrorCode.BR_USING_DELETED, meta, "Can not use deleted entity");
    }

    public static BadRequestException byExpirationBefore(Instant expiration, Instant notBefore) {
        Assert.notNull(expiration, "'expiration' must not be null");
        Assert.notNull(notBefore, "'notBefore' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("expiration", expiration);
        meta.put("notBefore", notBefore);
        return new BadRequestException(ErrorCode.BR_EXPIRATION_BEFORE_NOT_BEFORE, meta, "'expiration' can not be before from 'notBefore'");
    }

    public static BadRequestException byFromAfterTo(Instant from, Instant to) {
        Assert.notNull(from, "'from' must not be null");
        Assert.notNull(to, "'to' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("notBefore", from);
        meta.put("to", to);
        return new BadRequestException(ErrorCode.BR_FROM_AFTER_TO, meta, "'from' can not be after from 'to'");
    }

    public static BadRequestException byCreatingSuperUser() {
        return new BadRequestException(ErrorCode.BR_ASSIGN_SUPER, null, "Can not create user with role 'super'");
    }

    public static BadRequestException byExistingEmail(String email) {
        Assert.hasText(email, "'email' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("email", email);
        return new BadRequestException(ErrorCode.BR_EXISTING_EMAIL, meta, "Already exist a user with this email");
    }

    public static BadRequestException bySuspendedUser(User user) {
        Map<String, Object> u = ofUser(user);
        u.put("deleted", user.getDeleted());
        return new BadRequestException(ErrorCode.BR_SUSPENDED_ACCOUNT, u, "This user has been suspended");
    }

    public static BadRequestException byDeletingSuper() {
        return new BadRequestException(ErrorCode.BR_DELETE_SUPER, null, "Users with role 'super' can not be deleted");
    }

    public static BadRequestException byInvalidSortParam(Class<?> clazz, String param) {
        Assert.notNull(clazz, "'clazz' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", clazz.getSimpleName());
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_INVALID_SORT_PARAM, meta, "Invalid sort param");
    }

    public static BadRequestException byMalformedBody() {
        return new BadRequestException(ErrorCode.BR_MALFORMED_BODY, null, "Unable to parse body content");
    }

    public static BadRequestException byMalformedParam(MethodArgumentTypeMismatchException e) {
        Map<String, Object> meta = null;
        if (e != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", e.getName() != null ? e.getName() : e.getPropertyName());
            meta.put("value", e.getValue());
            meta.put("requiredType", e.getRequiredType() != null ? e.getRequiredType().getSimpleName() : "UNKNOWN");
            meta.put("message", e.getMessage());
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    public static BadRequestException byMalformedSortingParam(String message) {
        Map<String, Object> meta = null;
        if (message != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", "sort");
            meta.put("message", message);
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    public static BadRequestException fromAuthClient(AuthException ae) {
        Map<String, Object> meta = ae.getMeta() != null ? ae.getMeta() : new LinkedHashMap<>();
        meta.put("originalErrorCode", ae.getErrorCode());
        return new BadRequestException(ErrorCode.BR_AUTH_CLIENT, meta, ae.getMessage());
    }

    public static BadRequestException byExceedingMaxElements(Grouping grouping, long maxElements) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("grouping", ofEntity(grouping));
        meta.put("maxElements", maxElements);
        return new BadRequestException(ErrorCode.BR_MAX_GROUPING_ELEMENTS, meta, "Grouping exceeded max amount of elements");
    }

    public static BadRequestException byExceedingMaxCurrencies(Grouping grouping, Set<Currency> currencies) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("grouping", ofEntity(grouping));
        meta.put("currencies", currencies.stream().map(BadRequestException::ofEntity).collect(Collectors.toList()));
        return new BadRequestException(ErrorCode.BR_MAX_GROUPING_CURRENCIES, meta, "Grouping exceeded max amount of distinct currencies");
    }

    public static Map<String, Object> ofEntity(Entity entity) {
        Assert.notNull(entity, "'entity' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", entity.getClass().getSimpleName());
        meta.put("id", entity.getId());
        return meta;
    }

    public static Map<String, Object> ofFilterable(Filterable filterable) {
        Map<String, Object> meta = ofEntity(filterable);
        meta.put("owner", ofUser(filterable.getOwner()));
        return meta;
    }

    private static Map<String, Object> ofUser(User user) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("id", user.getId());
        meta.put("name", user.getName());
        meta.put("authId", user.getAuthId());
        return meta;
    }

}
