package com.fsquiroz.movements.exception;

import com.fsquiroz.movements.entity.db.AccountPermission;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends AppException {

    private NotFoundException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static NotFoundException byId(Class<?> clazz, Object id) {
        Map<String, Object> meta = ofClass(clazz);
        meta.put("id", id);
        return new NotFoundException(ErrorCode.NF_BY_ID, meta, "Entity not found");
    }

    public static NotFoundException byId(AccountPermission.PermissionId id) {
        Map<String, Object> meta = ofClass(AccountPermission.class);
        meta.put("forAccount", BadRequestException.ofEntity(id.getAccount()));
        meta.put("forUser", BadRequestException.ofEntity(id.getUser()));
        return new NotFoundException(ErrorCode.NF_BY_ID, meta, "Permission not found");
    }

    public static NotFoundException byEmail(Class<?> clazz, String email) {
        Map<String, Object> meta = ofClass(clazz);
        meta.put("email", email);
        return new NotFoundException(ErrorCode.NF_BY_EMAIL, meta, "Entity not found");
    }

    private static Map<String, Object> ofClass(Class<?> clazz) {
        Assert.notNull(clazz, "'clazz' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", clazz.getSimpleName());
        return meta;
    }

}
