package com.fsquiroz.movements.exception;

import com.fsquiroz.movements.entity.db.User;
import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends AppException {

    private UnauthorizedException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static UnauthorizedException byInvalidCredentials() {
        return new UnauthorizedException(ErrorCode.U_INVALID_CREDENTIALS, null, "Invalid credentials");
    }

    public static UnauthorizedException byDeletedUser(User user) {
        Assert.notNull(user, "'user' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("id", user.getId());
        meta.put("name", user.getName());
        meta.put("authId", user.getAuthId());
        meta.put("deleted", user.getDeleted());
        return new UnauthorizedException(ErrorCode.U_SUSPENDED_USER, meta, "Can not get a suspended user");
    }

    public static UnauthorizedException byInsufficientPermission() {
        return new UnauthorizedException(ErrorCode.U_INSUFFICIENT_PERMISSIONS, null, "Insufficient permission");
    }

    public static UnauthorizedException fromAuthClient(AuthException ae) {
        Map<String, Object> meta = ae.getMeta() != null ? ae.getMeta() : new LinkedHashMap<>();
        meta.put("originalErrorCode", ae.getErrorCode());
        return new UnauthorizedException(ErrorCode.U_AUTH_CLIENT, meta, ae.getMessage());
    }

}
