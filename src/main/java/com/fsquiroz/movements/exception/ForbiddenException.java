package com.fsquiroz.movements.exception;

import com.fsquiroz.movements.integration.fsauth.api.AuthException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbiddenException extends AppException {

    private ForbiddenException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static ForbiddenException fromAuthClient(AuthException ae) {
        Map<String, Object> meta = ae.getMeta() != null ? ae.getMeta() : new LinkedHashMap<>();
        meta.put("originalErrorCode", ae.getErrorCode());
        return new ForbiddenException(ErrorCode.FORBIDDEN, meta, ae.getMessage());
    }

}
