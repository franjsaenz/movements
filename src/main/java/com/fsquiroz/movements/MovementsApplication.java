package com.fsquiroz.movements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
        @PropertySource(value = "classpath:application.properties")
        ,
        @PropertySource(value = "file:${user.home}${file.separator}.movements${file.separator}application.properties")
})
public class MovementsApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MovementsApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(MovementsApplication.class, args);
    }

}
