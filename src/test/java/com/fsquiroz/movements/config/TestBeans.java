package com.fsquiroz.movements.config;

import com.fsquiroz.movements.entity.api.MStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import java.time.Instant;

@Configuration
@Profile("test")
@Slf4j
public class TestBeans {

    private MStatus status;

    @Bean
    @Primary
    public MStatus status() {
        if (status == null) {
            String title = "Movements";
            Instant now = Instant.now();
            String version = "TESTING";
            status = new MStatus(title, version, now, now, "Up");
        }
        return status;
    }

}
